package ru.t1.chubarov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public final class UserProfileResponse extends AbstractUserResponse {

    @Nullable
    private UserDTO user;

    public UserProfileResponse(@Nullable final UserDTO user) {
        this.user = user;
    }

}
