package ru.t1.chubarov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.chubarov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.chubarov.tm.api.service.dto.ISessionDtoService;
import ru.t1.chubarov.tm.exception.entity.ModelNotFoundException;
import ru.t1.chubarov.tm.exception.entity.SessionNotFoundException;
import ru.t1.chubarov.tm.exception.field.IdEmptyException;
import ru.t1.chubarov.tm.exception.field.UserIdEmptyException;
import ru.t1.chubarov.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Service
public class SessionDtoService implements ISessionDtoService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    public ISessionDtoRepository getSessionRepository() {
        return context.getBean(ISessionDtoRepository.class);
    }

    @NotNull
    @Override
    public SessionDTO add(@Nullable String userId, @Nullable SessionDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull final ISessionDtoRepository repository = getSessionRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll() throws Exception {
        @NotNull final ISessionDtoRepository repository = getSessionRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll(@NotNull String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ISessionDtoRepository repository = getSessionRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAllByUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public SessionDTO findOneById(@NotNull String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final ISessionDtoRepository repository = getSessionRepository();
        try {
            @Nullable final SessionDTO model = repository.findOneByIdByUser(userId, id);
            if (model == null) throw new SessionNotFoundException();
            return model;
        } catch (@NotNull final Exception e) {
            throw e;
        }
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        @NotNull final ISessionDtoRepository repository = getSessionRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public SessionDTO remove(@NotNull final String userId, @Nullable SessionDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull final ISessionDtoRepository repository = getSessionRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeOneById(userId, model.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public void removeAll(@NotNull final String userId, @Nullable final Collection<SessionDTO> models) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ISessionDtoRepository repository = getSessionRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            for (@NotNull final SessionDTO session : models) {
                repository.removeOneById(userId, session.getId());
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public SessionDTO removeOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable SessionDTO model = new SessionDTO();
        model.setId(id);
        @NotNull final ISessionDtoRepository repository = getSessionRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeOneById(userId, model.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @NotNull
    @Override
    public SessionDTO removeOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable SessionDTO model = new SessionDTO();
        model.setId(id);
        @NotNull final ISessionDtoRepository repository = getSessionRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public int getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ISessionDtoRepository repository = getSessionRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSizeByUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int getSize() throws Exception {
        @NotNull final ISessionDtoRepository repository = getSessionRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

}
