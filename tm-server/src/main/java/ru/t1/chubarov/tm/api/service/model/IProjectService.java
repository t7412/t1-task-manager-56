package ru.t1.chubarov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    void create(@Nullable String userId, @NotNull String name) throws Exception;

    void create(@Nullable String userId, @NotNull String name, @NotNull String description) throws Exception;

    void add(@Nullable Project model) throws Exception;

    void addByUserId(@Nullable String userId, @Nullable Project model) throws Exception;

    void updateById(@Nullable String userId, @Nullable String id, @NotNull String name, @NotNull String description) throws Exception;

    void changeProjectStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    List<Project> findAll(@Nullable String userId) throws Exception;

    @NotNull
    List<Project> findAll() throws Exception;

    @NotNull
    Project findOneById(@NotNull String userId, @Nullable String id) throws Exception;

    void remove(@Nullable String userId, @Nullable Project model) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    void removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    long getSize();

    Long getSize(@Nullable String userId) throws Exception;

    void addAll(@NotNull Collection<Project> models) throws Exception;

    void clear();
}
