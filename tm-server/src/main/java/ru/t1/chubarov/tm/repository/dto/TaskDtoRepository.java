package ru.t1.chubarov.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.chubarov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.chubarov.tm.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
public class TaskDtoRepository extends AbstractUserOwnerDtoRepository<TaskDTO> implements ITaskDtoRepository {

    @NotNull
    @Autowired
    public EntityManager entityManager;

    @Nullable
    @Override
    public List<TaskDTO> findAll() {
        return entityManager
                .createQuery("SELECT p FROM TaskDTO p ", TaskDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByUser(@Nullable String userId) {
        return entityManager
                .createQuery("SELECT p FROM TaskDTO p WHERE p.userId = :userId", TaskDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@NotNull String id) {
        return entityManager
                .createQuery("SELECT p FROM TaskDTO p WHERE p.id = :id", TaskDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("id", id)
                .setFirstResult(0)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public TaskDTO findOneByIdByUser(@Nullable String userId, @Nullable String id) {
        return entityManager
                .createQuery("SELECT p FROM TaskDTO p WHERE p.userId = :userId AND p.id = :id", TaskDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setFirstResult(0)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM TaskDTO").executeUpdate();
    }

    @Override
    public void removeAll(@Nullable String userId) {
        entityManager
                .createQuery("DELETE FROM TaskDTO p WHERE p.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void remove(@Nullable String userId, @NotNull TaskDTO model) {
        entityManager
                .createQuery("DELETE FROM TaskDTO p WHERE p.userId = :userId and p.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", model.getId())
                .executeUpdate();
    }

    @Override
    public void removeOneById(@Nullable String userId, @Nullable String id) {
        entityManager
                .createQuery("DELETE FROM TaskDTO p WHERE p.userId = :userId and p.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public int getSize() {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM TaskDTO p", Integer.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public int getSizeByUser(@Nullable String userId) {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM TaskDTO p WHERE p.userId = :userId", Integer.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @NotNull
    @Override
    public Boolean existsById(@Nullable String userId, @Nullable String id) {
        return entityManager
                .createQuery("SELECT COUNT(p) = 1 FROM TaskDTO p WHERE p.userId = :userId", Boolean.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Nullable
    @Override
    public TaskDTO findOneByIndex(@NotNull Integer index) {
        return entityManager
                .createQuery("SELECT p FROM TaskDTO p WHERE p.id = :id", TaskDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("id", index)
                .setFirstResult(0)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeOneByIndex(@Nullable String userId, @Nullable Integer index) {
        entityManager
                .createQuery("DELETE FROM TaskDTO p WHERE p.userId = :userId and p.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", index)
                .executeUpdate();
    }

}
